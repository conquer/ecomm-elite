import Ember from 'ember';
import Paginated from 'ecomm-elite/mixins/paginated-route';

export default Ember.Route.extend(Paginated, {
  model() {
    return this.get('store').findAll('email-message');
  }
});
