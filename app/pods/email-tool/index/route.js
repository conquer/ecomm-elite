import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return Ember.RSVP.hash({
      emailData: this.get('store').findAll('email-tool-datum'),
      emailMessages: this.get('store').findAll('email-message')
    });
  }
});
