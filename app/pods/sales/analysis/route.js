import Ember from 'ember';
import Paginated from 'ecomm-elite/mixins/paginated-route';

export default Ember.Route.extend(Paginated, {

  model(params) {
    return this.store.query('sales-analysis', { page: params.page });
  }


});
