import Ember from 'ember';
import Paginated from 'ecomm-elite/mixins/paginated-route';

export default Ember.Route.extend(Paginated, {
  model() {
    return this.get('store').findAll('upc-upload');
  },

  actions: {
    delete(record) {
      record.destroyRecord();
    },

    create(csvFile) {
      let controller = this.controllerFor('upc-tool');
      controller.set('uploading', true);
      this.get('store').createRecord('upc-upload', {
        fileName: csvFile
      }).save().then(() => {
        controller.set('csvFile', '');
        controller.set('uploading', false);
      });
    }

  }
});
