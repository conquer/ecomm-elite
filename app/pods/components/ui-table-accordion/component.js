import Ember from 'ember';
import Base from 'semantic-ui-ember/mixins/base';

export default Ember.Component.extend(Base, {
  module: 'accordion',
  classNames: ['ui', 'accordion', 'table'],
  tagName: 'tbody',

});
