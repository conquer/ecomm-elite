import Ember from 'ember';

export default Ember.Component.extend({
  sortService: Ember.inject.service(),

  tagName: 'div',
  classNames: ['dimmable'],

  sortedModelAll: Ember.computed.sort('model', 'sortDefinition'),

  sortedModel: Ember.computed('sortedModelAll', 'searchFilter', function() {
    let sortedModel = this.get('sortedModelAll');

    let regex = new RegExp(this.get('searchFilter'), 'gi');

    if (this.get('searchFilter') && this.get('filterProperty')) {
      return sortedModel.filter((item) => {
        return regex.test(item.get(this.get('filterProperty')));
      });
    } else {
      return sortedModel;
    }

  }),

  sortDefinition: Ember.computed('sortService.sortBy', 'sortService.reverseSort', function() {
    let sortOrder = this.get('sortService.reverseSort') ? 'desc' : 'asc';
    return [ `${this.get('sortService.sortBy')}:${sortOrder}` ];
  }),

  pageInt: Ember.computed('page', function() {
    return parseInt(this.get('page'), 10);
  }),

  totalPagesInt: Ember.computed('model.meta.totalpages', function() {
    return parseInt(this.get('model.meta.totalpages'), 10);
  }),

  pageLinks: Ember.computed('totalPagesInt', function() {
    let totalPages = this.get('totalPagesInt');
    let pageLinks = [];
    let elipleft = false, elipright = false;
    let currentPage = this.get('pageInt');

    let rangeLength = 8;
    let rangeStart = currentPage - (rangeLength/2);
    let rangeEnd = currentPage + (rangeLength/2);

    if (rangeStart - 1 > 0  && rangeLength < totalPages) {
      elipleft = true;
    } else {
      rangeStart = 0;
    }

    if (rangeEnd + 1 < totalPages && rangeLength < totalPages) {
      elipright = true;
      // Default build from right
      for (let i = 0; i <= rangeLength; i++) {
        if (rangeStart + i <= totalPages) {
          pageLinks[i] = { value: rangeStart + i, label: rangeStart + i + 1 };
        }
      }
    } else {
      // Within range of total pages, build from left
      for (let i = rangeLength; i >= 0; i--) {
        if (totalPages - i >= 0) {
          pageLinks[i] = { value: totalPages - i, label: totalPages - i + 1 };
        }
      }
      pageLinks.reverse();
    }

    if (elipleft) {
      pageLinks.unshift({ elip: true });
      pageLinks.unshift({ value: 0, label: 1 });
    }
    if (elipright) {
      pageLinks.push({ elip: true });
      pageLinks.push({ value: totalPages, label: totalPages + 1 });
    }

    return pageLinks;
  }),

  willRender() {
    this._super(...arguments);
    if (!this.get('page')) {
      this.set('page', 0);
    }
  },

  actions: {
    setPage(number) {
      this.set('page', number);
    },
    nextPage() {
      if (this.get('pageInt') < this.get('totalPagesInt')) {
        this.incrementProperty('page');
      }
    },
    previousPage() {
      if (this.get('pageInt') > 0) {
        this.decrementProperty('page');
      }
    }
  }

});
