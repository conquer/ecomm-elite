import Ember from 'ember';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3TimeFormat from 'd3-time-format';
import * as d3Selection from 'd3-selection';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as d3Transition from 'd3-transition';

const d3 = Object.assign({}, d3Scale, d3Shape, d3TimeFormat, d3Selection, d3Array, d3Axis, d3Transition);

export default Ember.Component.extend({
  classNames: ['line-chart'],

  //create the graph initially after the component is inserted into the DOM
  didInsertElement() {

    let domWidth = Ember.$('svg').css('width', '100%').width();

    // set the dimensions and margins of the graph
    let margin = { top: 20, right: 20, bottom: 70, left: 50 },
        width = domWidth - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

    // set the ranges
    let x = d3.scaleTime().range([0, width]);
    let y = d3.scaleLinear().range([height, 0]);

    // define the line
    let valueline = d3.line()
        .x(function(d) { return x(d.date); })
        .y(function(d) { return y(d.orders); })
        .curve(d3.curveBasis);

    let valueline2 = d3.line()
        .x(function(d) { return x(d.date); })
        .y(function(d) { return y(d.messages); })
        .curve(d3.curveBasis);

    // append the svg object to the body of the page
    // appends a 'group' element to 'svg'
    // moves the 'group' element to the top left margin
    let svg = d3.select("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // Get the data
    let data = this.get('data').map(function(dat) {
      let date = dat.get('date');
      let orders = dat.get('orders');
      let messages = dat.get('messages');

      return { date, orders, messages };
    });

    data = data.sortBy('date');

      // Scale the range of the data
      x.domain(d3.extent(data, function(d) { return d.date; }));
      y.domain([0, d3.max(data, function(d) { return d.orders; })]);

      // d3.select("svg").transition()
      // .duration(750)
      // .ease(d3.easeLinear);

      // Add the valueline path.
      svg.append("path")
          .data([data])
          .attr("class", "line")
          .attr("d", valueline);

      svg.append("path")
          .data([data])
          .attr("class", "line2")
          .attr("d", valueline2);
      // Add the X Axis
      svg.append("g")
          .attr("class", "axis")
          .attr("transform", "translate(0," + height + ")")
          .call(d3.axisBottom(x).ticks(10))
          .selectAll("text")
            .style("text-anchor", "end")
            .attr("dx", "-.8em")
            .attr("dy", ".15em")
            .attr("transform", "rotate(-65)");

      // Add the Y Axis
      svg.append("g")
          .attr("class", "axis")
          .call(d3.axisLeft(y).ticks(20));

  }

});
