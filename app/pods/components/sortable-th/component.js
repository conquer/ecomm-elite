import Ember from 'ember';

const SortableThComponent = Ember.Component.extend({
  sortService: Ember.inject.service(),

  tagName: 'th',
  classNameBindings: ['sorted', 'reverseSort:ascending:descending'],
  attributeBindings: ['colspan'],
  sorted: Ember.computed('sortService.sortBy', function() {
    return this.get('sortService.sortBy') === this.get('propertyName');
  }),
  reverseSort: Ember.computed.alias('sortService.reverseSort'),

  click() {
    if (this.get('sortService.sortBy') === this.get('propertyName')) {
      this.toggleProperty('sortService.reverseSort');
    } else {
      this.set('sortService.reverseSort', false);
    }
    this.set('sortService.sortBy', this.get('propertyName'));
  }

});

SortableThComponent.reopenClass({
  positionalParams: ['propertyName']
});

export default SortableThComponent;
