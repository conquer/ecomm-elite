import Ember from 'ember';

export default Ember.Component.extend({
  tagName:'',

  actions: {
    trigger() {
        let modalName = this.get('modalName');
        if (modalName) {
          Ember.$('.ui.' + modalName + '.modal').modal('show');
        } else {
          throw new Error('Modal trigger requires "modalName" attr.');
        }
    }
  }
});
