import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('sales', function() {
    this.route('analysis');
    this.route('comparison');
    this.route('customers');
    this.route('pending-orders');
    this.route('activity');
    this.route('profit');
  });
  this.route('resources');
  this.route('events');
  this.route('keywords', function() {
    this.route('tracker');
    this.route('finder', function() {
      this.route('search-history');
    });
    this.route('optimizer');
  });
  this.route('upc-tool');
  this.route('code-feeder');
  this.route('email-tool', function() {
    this.route('messages');
    this.route('campaigns');
    this.route('queued');
    this.route('sent');
    this.route('blacklist');
    this.route('settings');
  });
});

export default Router;
