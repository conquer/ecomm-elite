import DS from 'ember-data';

export default DS.Model.extend({
  orders: DS.attr('number'),
  messages: DS.attr('number'),
  date: DS.attr('date')
});
