import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  email: DS.attr('string'),
  orders: DS.attr('number'),
  address: DS.attr('string'),
  city: DS.attr('string'),
  state: DS.attr('string'),
  zip: DS.attr('string'),
  phone: DS.attr('string'),
  latestPurchaseDate: DS.attr('date'),
  LTV: DS.attr('string'),
  unitsOrdered: DS.attr('string'),
  numberOfOrders: DS.attr('string')

});
