import DS from 'ember-data';

export default DS.Model.extend({
  orderId: DS.attr('string'),
  purchaseDate: DS.attr('date'),
  loadedOn: DS.attr('date'),
  status: DS.attr('string'),
  asins: DS.attr('string'),
  skus: DS.attr('string'),
  totalQuantity: DS.attr('number')
});
