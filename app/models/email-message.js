import DS from 'ember-data';

export default DS.Model.extend({
  product: DS.attr('string'),
  time: DS.attr('date'),
  message: DS.attr('string'),
  campaign: DS.attr('string'),
  orderId: DS.attr('string'),
  name: DS.attr('string'),
  to: DS.attr('string'),
  subject: DS.attr('string'),
  body: DS.attr('string')
});
