import DS from 'ember-data';

export default DS.Model.extend({
  productName: DS.attr('string'),
  sku: DS.attr('string'),
  averageItemPrice: DS.attr('number'),
  currentItemPrice: DS.attr('number'),
  amazonReferralFee: DS.attr('number'),
  amazonFulfilment: DS.attr('number'),
  costPerUnit: DS.attr('number'),
  weightHandlingStorage: DS.attr('number'),
  ppcCosts: DS.attr('number'),
  miscCosts: DS.attr('number'),
  unitProfit: DS.attr('number'),
  profit: DS.attr('number')
});
