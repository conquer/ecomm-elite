import DS from 'ember-data';

export default DS.Model.extend({
  productName: DS.attr('string'),
  sku: DS.attr('string'),

  unitsDate1: DS.attr('number'),
  unitsDate2: DS.attr('number'),
  unitsStatus: DS.attr('number'),

  salesDate1: DS.attr('number'),
  salesDate2: DS.attr('number'),
  salesStatus: DS.attr('number'),

  profitDate1: DS.attr('number'),
  profitDate2: DS.attr('number'),
  profitStatus: DS.attr('number'),

});
