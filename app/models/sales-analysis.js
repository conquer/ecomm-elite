import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  sku: DS.attr('string'),
  numberOfUnitItems: DS.attr('string'),
  unitItemsPercent: DS.attr('string'),
  saleAmount: DS.attr('string'),
  saleAmountPercent: DS.attr('string'),
  avgUnitProfit: DS.attr('string'),
  profit: DS.attr('string'),
  latestPurchaseDate: DS.attr('date'),
});
