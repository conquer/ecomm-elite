import DS from 'ember-data';

export default DS.Model.extend({
  fileName: DS.attr('string'),
  resultFile: DS.attr('string'),
  completedAt: DS.attr('date')
});
