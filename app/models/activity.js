import DS from 'ember-data';

export default DS.Model.extend({
  orderId: DS.attr('string'),
  updatedAt: DS.attr('date'),
  purchaseDate: DS.attr('date'),
  status: DS.attr('string')
});
