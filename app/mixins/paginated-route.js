import Ember from 'ember';

export default Ember.Mixin.create({
  queryParams: {
     page: { refreshModel: true },
     startDate: { refreshModel: true },
     endDate: { refreshModel: true }
   },

   setupController(model, controller) {
     this._super(...arguments);
     controller.setProperties({
       queryParams: ['page'],
       page: null,
       startDate: null,
       endDate: null
     });
   },

   actions: {
     loading(transition, originRoute) {
       let controller = this.controllerFor(originRoute.routeName);
       controller.set('modelLoading', true);
       transition.promise.finally(function() {
           controller.set('modelLoading', false);
       });
     }
   }
});
