import { JSONAPISerializer } from 'ember-cli-mirage';
import Ember from 'ember';

export default JSONAPISerializer.extend({
  serialize(response, request) {
  let json = JSONAPISerializer.prototype.serialize.apply(this, arguments);

  let page = parseInt(request.queryParams.page, 10) || 0;
  let pageLength = 10;

  let totalPages = parseInt(json.data.length / 10, 10);

  if (Ember.isArray(json.data)) {
    json.data = json.data.slice(page * pageLength, (page * pageLength) + pageLength);

    json.meta = {
      'totalpages': totalPages
    };
  }
  return json;
}
});
