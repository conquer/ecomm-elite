export default function() {
  this.urlPrefix = 'http://localhost:4200';
  this.namespace = 'api';

  // Reports
  this.get('/sales-analyses');
  this.get('/sales-comparisons');
  this.get('/customers');
  this.get('/pending-orders');
  this.get('/activities');
  this.get('/profits');
  this.get('/email-tool-data');
  this.get('/email-messages');

  // CRUD
  this.get('/upc-uploads');
  this.del('/upc-uploads/:id');
  this.post('/upc-uploads');

  this.post('/upc-uploads', function(schema, request) {
    let attrs = this.normalizedRequestAttrs();
    let file = attrs.fileName;

    // Mock conversion (change file name)
    attrs.fileName = file+'.csv';
    attrs.resultFile = file+'-results.xlsx';
    attrs.completedAt = new Date();

    return schema.upcUploads.create(attrs);
  }, { timing: 2000 }); // Mock upload time

  /*
    Shorthand cheatsheet:

    this.get('/posts');
    this.post('/posts');
    this.get('/posts/:id');
    this.put('/posts/:id'); // or this.patch
    this.del('/posts/:id');

    http://www.ember-cli-mirage.com/docs/v0.2.x/shorthands/
  */
}
