export default function(server) {

  /*
    Seed your development database using your factories.
    This data will not be loaded in your tests.

    Make sure to define a factory for each model you want to create.
  */

  server.createList('sales-analysis', 67);
  server.createList('sales-comparison', 72);
  server.createList('customer', 32);
  server.createList('pending-order', 8);
  server.createList('activity', 432);
  server.createList('profit', 43);
  server.createList('upc-upload', 8);
  server.createList('email-tool-datum', 34);
  server.createList('email-message', 14);
}
