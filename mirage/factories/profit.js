import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  productName() { return faker.commerce.productName(); },
  sku() { return faker.random.uuid().substr(5, 12); },
  averageItemPrice() { return faker.commerce.price(); },
  currentItemPrice() { return faker.commerce.price(); },
  amazonReferralFee() { return faker.commerce.price(); },
  amazonFulfilment() { return faker.commerce.price(); },
  costPerUnit() { return faker.commerce.price(); },
  weightHandlingStorage() { return faker.commerce.price(); },
  ppcCosts() { return faker.commerce.price(); },
  miscCosts() { return faker.commerce.price(); },
  unitProfit() { return faker.commerce.price(); },
  profit() { return faker.commerce.price(); }
});
