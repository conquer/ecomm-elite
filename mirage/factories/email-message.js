import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  product: function () { return faker.commerce.productName(); },
  time: function () { return faker.date.past(1); },
  message: function () { return "Generic - Seller Feedback Ask v2" },
  campaign: function () { return "Generic - Seller Feedback Ask" },
  orderId: function () { return faker.random.uuid().substr(5, 12); },
  name: function () { return faker.name.findName(); },
  to: function () { return faker.internet.email(); },
  subject: function () { return faker.lorem.words(4).join(' '); },
  body: function () { return faker.lorem.words(40).join(' '); },
});
