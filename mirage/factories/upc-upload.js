import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  tmpFileName() { return faker.lorem.words(2).join('_'); },
  fileName() { return this.tmpFileName+'.csv'; },
  resultFile() { return this.tmpFileName+'-results.xlsx'; },
  completedAt() { return faker.date.past(1); },
});
