import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  orderId() { return faker.random.uuid().substr(5, 12); },
  purchaseDate() { return faker.date.past(1); },
  loadedOn() { return faker.date.past(1); },
  status() { return faker.random.boolean(); },
  asins() { return faker.random.uuid().substr(5, 12); },
  skus() { return faker.random.uuid().substr(5, 12); },
  totalQuantity() { return faker.random.number(100); }
});
