import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  productName() { return faker.commerce.productName(); },
  sku() { return faker.random.uuid().substr(5, 12); },

  unitsDate1() { return faker.random.number(500); },
  unitsDate2() { return faker.random.number(500); },
  unitsStatus() { return faker.random.number(500); },

  salesDate1() { return faker.commerce.price(); },
  salesDate2() { return faker.commerce.price(); },
  salesStatus() { return faker.random.number(500); },

  profitDate1() { return faker.commerce.price(); },
  profitDate2() { return faker.commerce.price(); },
  profitStatus() { return faker.random.number(500); },
});
