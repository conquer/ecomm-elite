import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  orderId() { return faker.random.uuid().substr(5, 12);  },
  updatedAt() { return faker.date.past(1); },
  purchaseDate() { return faker.date.past(1); },
  status() { return faker.random.arrayElement(["Pending", "Shipped"]); }
});
