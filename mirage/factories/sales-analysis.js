import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  name() { return faker.commerce.productName(); },
  sku() { return faker.random.uuid().substr(5, 12); },
  numberOfUnitItems() { return faker.random.number(49); },
  unitItemsPercent() { return faker.random.number(100); },
  saleAmount() { return faker.commerce.price(); },
  saleAmountPercent() { return faker.random.number(100); },
  avgUnitProfit() { return faker.commerce.price(); },
  profit() { return faker.commerce.price(); },
  latestPurchaseDate() { return faker.date.past(1); }
});
