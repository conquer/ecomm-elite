import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  orders: function () { return faker.random.number(1000); },
  messages: function () { return faker.random.number(1000); },
  date: function () { return faker.date.past(1); },
});
