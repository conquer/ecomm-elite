import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  name() { return faker.name.findName(); },
  email() { return faker.internet.email(); },
  orders() { return faker.random.number(); },
  address() { return faker.address.streetAddress(); },
  city() { return faker.address.city(); },
  state() { return faker.address.state(); },
  zip() { return faker.address.zipCode(); },
  phone() { return faker.phone.phoneNumber(); },
  latestPurchaseDate() { return faker.date.past(1); },
  LTV() { return faker.random.number(); },
  unitsOrdered() { return faker.random.number(); },
  numberOfOrders() { return faker.random.number(); }
});
